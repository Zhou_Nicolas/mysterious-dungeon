'''
@author:ZHOU Nicolas
@author:FOUQUET Etienne
@version:1.7.9
'''

from random import *
from tkinter import *
from tkinter.messagebox import *

hp=200        # point de vie du hero
mana=100      # point d'energie pour utiliser des sorts
atk=10        # pont d'attaque du hero
atk_multiplicateur = 1 # multiplicateur de degat
VITatk=0.7    # vitesse d'attaque du hero
endurance=100 # nb d'endurance
enduperdu=0.2 # nb d'endurance perdu a chaque pas
gold=0        # nb d'argent
niveau=1      # Niveau du donjon
hero_poison=False   # Si True : le hero est empoisonne, prendre une potion de vie annule le poison

#aptitude
special1=special2=special3=special4=special5=special6=special7=special8=0          #} portes AND
Sspecial=[special1,special2,special3,special4,special5,special6,special7,special8] #}
exp=0            # Experience
expneeded=400    # experience necessaire pour apprendre une aptitude de base
expneeded2=800   # experience necessaire pour apprendre une aptitude rang 2
expneeded3=1800  # experience necessaire pour apprendre une aptitude rang 3
expneeded4=1500  # experience necessaire pour apprendre une aptitude rang 4

sps=6            #centre du cadre des aptitudes
skillx=[0,0,-2,2,  -1,1,-1,1,-4,-4,4,4,  0,0,-6,6,  -4,-4,4,4] #coordonnees x des boutons
skilly=[-2,2,0,0,  -4,-4,4,4,-1,1,-1,1,  -6,6,0,0,  -4,4,-4,4] #coordonnees y des boutons

skill_name=[['atk+',expneeded],['hp+',expneeded],['atk speed +',expneeded],['mana +',expneeded]                                                                                  ,['atk++',expneeded2],['atk%',expneeded2],['hp++',expneeded2],['Shield',expneeded2],['atk speed ++',expneeded2], ['enemy atk speed -', expneeded2],['fire ball',expneeded2],['mana ++',expneeded2]                                                                         ,['atk x2',expneeded3],['hp regen',expneeded3],['evade trap',expneeded3],['mana regen',expneeded3]                                                                                             ,['detect trap',expneeded4],['wall break',expneeded4],['enemy atk -',expneeded4],['heal',expneeded4]]# Noms des aptitudes + cout

ButtonRank=['Rank1','Rank2','Rank3','Rank4','Rank5','Rank6','Rank7','Rank8','Rank9','Rank10','Rank11','Rank12','Rank13','Rank14','Rank15','Rank16','Rank17','Rank18','Rank19','Rank20']             # Noms des boutons

OPENaptitude=False # Pour ouvrir les branches d'aptitudes
atkm_multiplicateur=1    # le sort "enemy attack -" permet de reduire l'attaque du monstre
Vatkm_multiplicateur=1   # le sort 'enemy speed atk -"permet de reduire la vitesse d'attaque du monstre
heal_skill=False # il faut apprendre le sort "heal"
heal_cost=40     # Cout du sort heal
heal_hp=20       # Nombre hp gagne quand le sort heal est active
detect_trap_skill=False # il faut apprendre le sort "detect trap"
detect_cost=10   # Cout du sort detect trap
detected_trap=[] # Liste pout stocker tous les pieges detectes
wall_break_skill=False # il faut apprendre le sort "wall break"
wall_cost=70     # Cout du sort wall break
fire_skill=False # il faut apprendre le sort "fire ball"
fire_cost=50     # Cout du sort fire ball

hp_regen=False      # il faut apprendre le sort "heal regen"
mana_regen=False    # il faut apprendre le sort "mana regen"
shield_skill= False # il faut apprendre le sort "Shield"
anti_trap=0         # Variable augmente quand le sort "evade" est appris, le hero aura donc une chance d'esquiver une attaque
openskill=False     # Activer la fermeture de l'aptitude quand True

#Donjon

donjon_type = "normal" # Soit normal soit boss final
NBmonstre=8   # nombre de monstres dans chaque niveau (augmente en fonction du niveau)
NBpiege=5     # les pieges sont invisibles mais quand on y va dessus , il est vert
NBlignes=9    #}Dimension du cadre
NBcolonnes=9  #}
action=False #Pour la description, quand True, separe les comentaires

#boutique
arme_equip=0 # numéro de l'arme équipé 
arme=1 #numéro de l'arme affiché dans la boutique
arme_achete=[] # liste contenant toutes les armes déjà acheté
openshop=False  #Activer la fermeture du magasin quand True

## Creer les entitees
def entity_spawn():
    """
    Cette fonction sert a creer les entitees : les murs, le hero, le boss, les monstres avec leurs statistiques, les pieges et les      potions.
    Cependant, il y a deux voies a emprunter , c'est la variable donjon_type qui conchargee la voie.
    Si donjon_type est "normal", alors on va partir dans un donjon normal. Il y aura des patterns de murs, le hero, le boss, les monstres et les statistiques, les pieges et les potions, le tout creer sur des cases aleatoirement .
    """
    global Positions,PositionM,PositionB,PositionP, Position_pot, monstres , MScases ,Vatk , Typemonstre, xkey,ykey, action, tour, mmax, endurance, enduperdu, tour, niveau

    Vatk=VITatk    # On prend une autre variable de vitesse d'attaque pour ne pas changer la vitesse d'attaque de base

    Positions=[]     # Stocke tous les cases utilises pour ne pas les reutilises
    PositionM=[]     # Cases monstres utilises
    PositionB=[]     # Cases obstacles/murs utilises
    PositionP=[]     # Cases pieges utilises
    Position_pot=[]  # Cases des potions
    monstres=[]      # Liste stats des monstres
    xkey=ykey=0      # Coordonnees du personnage
    
    Typemonstre=[["Vampire",2,2,0,"lifesteal"], ["Ogre",20,2,-0.4], ["Goblin",0,-2,0.5,], ["Dragon",5,3,0.1,],["Cobra",5,-2,0,"evade","poison"],["Demon",0,1,0,"evade"],["Golem",10,7,-0.4]]                    #[Noms des monstre,stats specials: hp, atk, speed,la competence que le monstre a: vol de vie, esquive, poison]

    
    Pattern=[                                                   [11,13,15,17,21,23,25,27,31,33,35,37,41,43,45,47,51,53,55,57,61,63,65,67,71,73,75,77],[11,12,13,14,15,16,17,31,32,33,34,35,36,37,51,52,53,54,55,56,57,71,72,73,74,75,76,77],[11,12,13,15,16,17,21,27,31,33,35,37,51,53,55,57,61,67,71,72,73,75,76,77],                                                        [11,13,15,17,23,25,31,32,33,35,36,37,51,52,53,55,56,57,63,65,71,73,75,77],[11,12,13,15,16,17,21,27,31,33,34,35,37,41,47,51,53,54,55,57,61,67,71,72,73,75,76,77],[10,11,12,13,15,16,17,18,31,32,33,34,35,36,37,50,51,52,53,55,56,57,58,71,72,73,74,75,76,77]                ]#emplacements des obstacles

    # un des pattern est choisit
    if donjon_type == "normal":  # Donjon normal
        thepattern=choice(Pattern)
    else:                        # Donjon final
        thepattern = [11,12,13,15,16,17,21,27,31,33,34,35,37,43,45,51,53,54,55,57,61,67,71,72,73,75,76,77] #Pattern special contre le boss final
        
    #Creer les obstacles
    for case in thepattern:
        PositionB.append(case)
        Positions.append(case)
        block=Label(frame1,bg='grey').grid(column=case%10,row=case//10,sticky=N+S+W+E)

    #Emplacement aleatoire du hero
    """ Ici c'est la voie du donjon normal, pour que chaque unite (hero, boss, pieges, potions) est sur une case non utilisee, on cherche si la variable aleatoire est t-il dans Positions[], s'il y est alors on cree l'unite, sinon on recree une autre variable jusqu'a qu'elle n'est pas utilise."""
    if donjon_type == "normal" :                          
        while xkey==ykey==0:
            hero_rand=randrange(0,9)+10*randrange(0,9) #}
            if hero_rand not in Positions:             #}
                hero(hero_rand%10,hero_rand//10)       #}aleatoire sans qu'une meme case ait  2 objets differents
                xkey,ykey=hero_rand%10,hero_rand//10   #}
                Positions.append(hero_rand)            #}
                
    #Emplacement aleatoire du boss
        while monstres==[]:
            boss_rand=randrange(0,9)+10*randrange(0,9)
            if boss_rand not in Positions:
                monstres.append([30*niveau,5+niveau*5,0.5,["boss",0,0,0],boss_rand]) # Stats du boss
                Positions.append(boss_rand)
                PositionM.append(boss_rand)   
                
        #stats des monstres
        for x  in range(NBmonstre):
            monstres.append([randint(8,12)+niveau*4,randint(3,6)+niveau*2,round(uniform(0.5,0.8),2),choice(Typemonstre)])
            monstres[x+1][0]+=int(monstres[x+1][3][1]) 
            monstres[x+1][1]+=int(monstres[x+1][3][2]) # vie, atk, vitesse atk du monstre en plus ou en moins
            monstres[x+1][2]=monstres[x+1][2]+round((monstres[x+1][3][3]),2)
                                                                        
            
        #Creer les monstres et  les pieges
        mmax=1 #compter le nombre de cases deja attribue
        while len(PositionM)+len(PositionP) < NBmonstre+NBpiege:
            x=randrange(0,9)
            y=randrange(0,9)
            if x+10*y not in Positions:
                if len(PositionM) <NBmonstre+1: # Arrete de creer des monstres quand il y en a assez
                    Positions.append(x+10*y)
                    PositionM.append(x+10*y)
                    monstres[mmax].append(x+10*y)
                    monstre(mmax,x,y)
                #ajoute un piege dans positionP
                elif len(PositionP)<NBpiege:
                    Positions.append(x+10*y)
                    PositionP.append(x+10*y)
                mmax+=1
                
        #Creer des potions de vie et de mana
        while len(Position_pot) < 4:
            x=randrange(0,9)
            y=randrange(0,9)
            if x+10*y not in Positions:
                Positions.append(x+10*y)
                # potion de vie
                if len(Position_pot)<2 :
                    Position_pot.append([x+10*y,'hp_pot'])
                    Potion_case=Canvas(frame1,width=70,height=70)
                    Potion_case.create_oval(20,20,55,55,fill='red')                 #cercle rouge
                    Potion_case.create_text(37.5,35,text='hp',fill='orange')        #mot hp
                    Potion_case.grid(column=x , row=y , sticky = N+S+W+E)
                # potion de mana
                else:
                    Position_pot.append([x+10*y,'mana_pot'])
                    Potion_case=Canvas(frame1,width=70,height=70)
                    Potion_case.create_oval(20,20,55,55,fill='blue')                #cercle bleu
                    Potion_case.create_text(37,35,text='mana',fill='cyan')          #mot mana
                    Potion_case.grid(column=x , row=y , sticky = N+S+W+E)  
                       
        """Sinon on sera dans le donjon final, l'endurance revient au maximum mais se perd plus facilement, il y a un pattern special pour l'emplacement des murs. 2 monstres apparaissent dès le début et le boss apparait au centre. """
    
    else:                 # Donjon final
        niveau=1          # retour au niveau 1
        endurance=100     # Endurance remplit
        enduperdu=0.5     # Perd plus d'endurance a chaque deplacement
        tour=0            # Compter le nombre de tours
        hero(0,0)         # le hero a la premiere case       
        Positions.append(0) # Ajoute la case du hero et du boss
        Positions.append(44)
        
        monstres.append([20,0,0,[" FINAL \n  BOSS",0,0,0],44])        # Les stats du boss
        boss()
        
        for x in range(2): # On donne des stats au monstres
            monstres.append([randint(5,10)+niveau*2,randint(4,7)+niveau*2,round(uniform(0.5,0.7),2),choice(Typemonstre)])
            monstres[x+1][0]+=int(monstres[x+1][3][1]) 
            monstres[x+1][1]+=int(monstres[x+1][3][2]) # vie, atk, vitesse atk du monstre en plus ou en moins
            monstres[x+1][2]=monstres[x+1][2]+round((monstres[x+1][3][3]),2)
            
        mmax=1 #compter le nombre de cases deja attribue
        while len(PositionM) < 2:  # Creer 2 monstres
            x=randrange(0,9)
            y=randrange(0,9)
            if x+10*y not in Positions:
                if len(PositionM) <2:
                    Positions.append(x+10*y)
                    PositionM.append(x+10*y)
                    monstres[mmax].append(x+10*y)
                    monstre(mmax,x,y)
                mmax+=1

                
##le hero est notre personnage

def hero(x,y):
    """ A chaque deplacement, cette fonction va creer l'image du hero. """
    hero=Canvas(frame1,width=70,height=70,bg="yellow")  # Image en jaune du hero
    hero.create_text(35,35,text="Hero",fill="red")      # Nom du hero
    hero.grid(column=x,row=y)
                
##le boss est l'ennemi a vaincre pour gagner
def boss():
    """ Lorsqu'on a tue plusieurs monstres, le boss apparait sur une case vide aleatoire qui est predefini dans entity_spawn.  
    Mais si c'est lors du donjon final, il est apparait directement au debut du donjon avec un emplacement predefini  """
    boss=Canvas(frame1,bg='red',width=70,height=70)            # Image en bleu du boss
    boss.create_text(35,35,text=monstres[0][3][0])             # Nom du boss
    boss.create_text(17,65,text="{}HP".format(monstres[0][0])) # vie du monstre 
    boss.create_text(58,65,text="{}AD".format(monstres[0][1])) # atk du monstre
    boss.grid(column=(monstres[0][4])%10,row=(monstres[0][4])//10)

## Creer la case du monstre
def monstre(nb,x,y):
    """Cette fonction sert a creer la case d'un monstre avec ces stats : nom, hp, atk"""
    MScases=Canvas(frame1,bg='cyan',width=70,height=70)
    MScases.create_text(35,35,text=monstres[nb][3][0],fill="purple" )          # Nom du monstre
    MScases.create_text(17,65,text="{}HP".format(monstres[nb][0]),fill='red')  # Hp du monstre
    MScases.create_text(58,65,text="{}AD".format(monstres[nb][1]),fill='red')  # Atk du monstre
    MScases.grid(column=x,row=y,sticky=N+S+W+E)
    
##Description des caracteristiques :hp, mana, atk, atk speed, endurance, niveau, gold et experience 
def statistique():
    """ Cette fonction sert a raffraichir les stats du hero a chaque deplacement. """
    stat=Label(frame2,text="STATS: \n\n hp={} \n mana={} \n atk={} \n atk speed={} \n STA={}/100 \n niveau:{} \n gold:{}\n exp={}".format(round(hp) ,mana,round(atk*atk_multiplicateur), round(VITatk*endurance/100,2),round(endurance,1),niveau,gold,exp)).grid(column=0,row=0,sticky=N)
##touche clavier (deplacement)
def keyboard(event):
    '''
    C'est une fonction qui sert au deplacement du hero et aux blocages des cases.
    les cases sont comptees en ajoutant xkey (abscisse du hero) et 10*ykey(ordonnee du hero) . On multiplie par 10 l'ordonnee car pour passer d'une case d'une ligne à la ligne en dessous avec le même abscisse ,on ajoute 10xkey.
    Pour bloquer les obstacles , on cherche dans PositionB si la case qu'on veut y aller est un obstacle, si cela en est un , alors le déplacement sera impossible.
    Chaque fois qu'on deplace, on cherche s'il y a un piege a l'endroit ou on est, il apparait s'il y en a un. Si on a appris des competences auparavant, des competences comme la regeneration des points de vie s'activent
    '''
    global xkey, ykey, hp, mana, monstres, endurance, anti_trap, openboss, niveau, In_history, exp, VITatk, atk, Position_pot, action, touche, hero_poison, tour, mmax 

    touche= event.keysym

    fleche=["Up","Down","Left","Right"] #noms des fleches directionnelles
    sort=['1','2','3','4']              #nom des boutons 1, 2, 3 et 4
    sort_unlock=[heal_skill, detect_trap_skill, wall_break_skill, fire_skill] # Liste des variables qui activent ou desactivent les fonction
    sort_def= [heal,detect_trap,wall_break,fire_ball] #Noms des fonctions des aptitudes
    
    deplacementx=[-1,1,0,0]             # Deplacement horizontale
    deplacementy=[0,0,-1,1]             # Deplacement verticale
    antiblock=[-10,10,-1,1]             # Bloquer le deplacement vers une case bloque
    trap=False                          # Pas de piege
    move=True                           # Possiblite de se deplacer
    
    
    #Le but est d'espacer chaque description a chaque deplacement
    if action == True:
        In_history.insert(END,"_____________________________________________\n") # Cela sert a informormer le joueur dans le texte a droite du cadre
        In_history.see("end") # Cela sert a voir le message sans defiler le texte
        action=False

    # SI c'est la touche 1, 2, 3 ou 4 qui est appuye, cela va activer le sort respective heal,detect,wall break et fireball
    if touche =='1' or touche =='2' or touche =='3' or touche =='4':
        for x,y,z in zip(sort, sort_unlock,sort_def) :
            if touche== x and y == True:
                z()    # Active le sort
            
    # SI c'est les touches directionnelles qui sont appuyer:    
    else:    
        for direction,plus,moins,l,interdit in zip(fleche , deplacementx ,deplacementy,range(0,4),antiblock):
            if touche == direction:
                # obstacles infranchissable
                for block in PositionB:
                    if block == (xkey+10*ykey)+interdit: # Si on ordonne le déplacement sur une case occupé pour un obstacle
                        move=False                       # Bloque le deplacement
                if move == False:
                    break
    
                #rester dans le cadre
                if ykey==0 and l ==0:                # Ne pas sortir de la face du haut
                    break
                elif ykey==NBlignes-1 and l == 1:    # Ne pas sortir de la face du bas
                    break
                elif xkey==0 and l == 2:             # Ne pas sortir de la face de gauche
                    break
                elif xkey==NBcolonnes-1 and l == 3:  # Ne pas sortir de la face de droite
                    break
                else:
                    #si la case est une piège: un piège apparrait
                    for piege in PositionP :
                        if xkey+10*ykey == piege:
                            piege=Canvas(frame1,bg="green",width=70,height=70) # Image en vert du piege
                            piege.create_text(35,35,text="Trap",fill="black")  # Nom du piege
                            piege.grid(column=xkey,row=ykey,sticky=N+S+W+E)
                            trap=True
                    if trap==False: # on efface la trace du personnage s'il n'y a pas de pieges
                        erase=Canvas(frame1,width=70,height=70)
                        erase.grid(column=xkey,row=ykey)
                        trap=False
                    hero(xkey+moins,ykey+plus) # Imprimer le personnage a la nouvelle l'emplacement
                    xkey=xkey+moins            #}Nouvelles coordonnees
                    ykey=ykey+plus             #}
    
                    #endurance est réduit à chaque pas
                    endurance=round(endurance-enduperdu,2)
                    
                    # Alerter le joueur lorsque l'endurance passe en dessous de 50 et de 10
                    if endurance==50.0:
                        In_history.insert(END,"ATTENTION! endurance: 50/100 \n") 
                        In_history.see("end")
                    if endurance==10.0:
                        In_history.insert(END,"ATTENTION!!! endurance: 10/100 \nVous devez vous battre contre le boss final!") 
                        In_history.see("end")
                        
        #Ouverture des pieges
        for piege in PositionP :
            if xkey+10*ykey == piege: #on cherche si le personnage est sur une case de piege
                detected_trap.append(piege) #Les pieges detectes
                if randint(0,anti_trap) ==1: #lorsque l'aptitude antitrap est appris , il y a une cahnce d'esquiver le piege
                    In_history.insert(END,"Vous avez evite le piege! \n") # Decrit chaque effet dans la description
                    In_history.see("end") # Cette ligne sert a voir le message sans deplacer avec la souris
                    action = True
                else: # Sinon , on perd des points de vie
                    hp-=5*niveau
                    In_history.insert(END,"Il y a un piege ! \n -{}HP \n".format(5*niveau)) 
                    In_history.see("end")
                    action = True
                piege=Canvas(frame1,bg="green",width=70,height=70)
                piege.create_text(35,35,text="Hero",fill="red")
                piege.grid(column=xkey,row=ykey,sticky=N+S+W+E)
    
        #Activer les potions:
        for potion in range(len(Position_pot)):
            if xkey+10*ykey == Position_pot[potion][0] : # Si c'est une potion de hp
                if Position_pot[potion][1] == 'hp_pot':
                    hp+=10*niveau                        # Gagne des points vie
                    In_history.insert(END,"Hp + {} ! \n".format(10*niveau))
                    In_history.see("end")
                    if hero_poison == True:               # Si le hero est empoisonne alors il est desempoissonne
                        hero_poison = False
                        In_history.insert(END,"Vous etes gueris du poison! \n")
                        In_history.see("end")
    
                else:                                    #Sinon: potion de mana
                    mana+=10*niveau                      # Gange des points de mana
                    In_history.insert(END,"Mana + {} ! \n".format(5*niveau))
                    In_history.see("end")
                    
                action = True
                Position_pot.remove(Position_pot[potion]) # On retire l'existence de la potion
                break
        
        if hp_regen==True:  #gagne du mana a chaque deplacement quand l'aptitude mana regen est appris
            hp+=1+round(niveau/3)
        if mana_regen==True:  #gagne du mana a chaque deplacement quand l'aptitude mana regen est appris
            mana+=1+round(niveau/3)
        
        # Si le hero est empoisonne par un monstre  (Cobra)
        if hero_poison == True:
            hp-=niveau
            In_history.insert(END,"HP - {}(Poison) \n".format(niveau))
            In_history.see("end")
            
        '''
        Mais, il y a des evenements speciaux lors du combat contre le boss final.
        Il y a un compteur de tour, tout les 4 tours, le hero perd des point de vie, tout les 5 tours, des monstres apparaissent dans le cadre, tout les 6 tours, les monstres deviennent plus fort et tout les 10 tours, des des potions apparaissent. 
        '''
        if donjon_type != "normal":  # Ce sont des parametres specifiques au donjon final
            tour = tour + 1 # systeme de tour qui augmente à chaque deplacement
        
            if tour%4 == 0: #tout les 4 tours, le hero perd des pv 
                hp = round(hp*0.95,0)
                In_history.insert(END,"Vous avez perdu de la vie \n car la salle est empoissone  \n")
                In_history.see("end")
                
            if tour%6 == 0:
                niveau= niveau+1  # tout les 5 tours, la difficulté augmentent, les monstres deviennent plus fort
                In_history.insert(END,"Le boss et de plus en plus en colère, \nle combat risque de devenir plus comliqué \n")
                In_history.see("end")
                
            if tour%10 == 0 and tour<=70: # Creer des potions: une de vie et une de mana, ne peut plus en creer a partie du tour 70
                mpot=0           # Nombre de potions crees
                while mpot < 2:  
                    x=randrange(0,9)
                    y=randrange(0,9)
                    if x+10*y not in Positions:
                        Positions.append(x+10*y)
                        # potion de vie
                        if mpot==0 : 
                            Position_pot.append([x+10*y,'hp_pot'])
                            Potion_case=Canvas(frame1,width=70,height=70)
                            Potion_case.create_oval(20,20,55,55,fill='red')                 #cercle rouge
                            Potion_case.create_text(37.5,35,text='hp',fill='orange')        #mot hp
                            Potion_case.grid(column=x , row=y , sticky = N+S+W+E)
                        # potion de mana
                        else:
                            Position_pot.append([x+10*y,'mana_pot'])
                            Potion_case=Canvas(frame1,width=70,height=70)
                            Potion_case.create_oval(20,20,55,55,fill='blue')                #cercle bleu
                            Potion_case.create_text(37,35,text='mana',fill='cyan')          #mot mana
                            Potion_case.grid(column=x , row=y , sticky = N+S+W+E) 
                        mpot+=1 # Ajoute une potion
                        
            if tour%5 == 0 and tour<=70:  # tout les 5 tours, des monstres apparaissent                
                                
                # Creer de nouveaux monstres 
                mm=0 # Nombre de monstres crees
                while mm < 2:
                    x=randrange(0,9)
                    y=randrange(0,9)
                    if x+10*y not in Positions:
                        if len(PositionM) <=mmax:
                            monstres.append([round(randrange(30,50)+niveau*10,0),round(randrange(10,15)+niveau*5,0), round(uniform(0.5,0.8),2),choice(Typemonstre)])
                            monstres[x+1][0]+=int(monstres[x+1][3][1]) 
                            monstres[x+1][1]+=int(monstres[x+1][3][2]) # vie, atk, vitesse atk du monstre en plus ou en moins
                            monstres[x+1][2]=monstres[x+1][2]+round((monstres[x+1][3][3]),2)
                            
                            Positions.append(x+10*y)
                            PositionM.append(x+10*y)
                            monstres[mmax].append(x+10*y)
                            monstre(mmax,x,y)
                        mmax+=1    # Sert a ajouter le numero de la case au nouveau monstre cree donc la derniere valeur dans monstres[]
                        mm = mm+1  # Ajoute un monstre
                In_history.insert(END,"le boss a invoqué des nouveaux monstres ! \n")
                In_history.see("end")
            
        # La fonction pour chercher s'il y a un combat
        battle()
    
##Combat contre monstre
def battle():
    """
    On cherche d'abord si la case est une case de monstre . Pour cela on a positionM qui contient pour les valeurs des monstres. Si la case du hero est l'un des cases de monstres alors cela lance le combat.
    Le combat est lié aux variables:Vatk et Vatkm , la vitesse d'attaque.
    Plus la vitesse d'attaque est grande , plus la frequence d'attaque est haute.
    On soustrait 1(Time_atk) par Vatk et 1 (Time_atkm) par Vatkm.
    Si le résultat de soit l'un soit l'autre est inférieur ou égal à 0:
        le personnage concerné pourra attaqué , cela mettra False en True.
    Si le résultat des deux est inférieur ou égal à 0:
        on compare celle du hero par celle du monstre:
            si le résultat est négatif,alors le hero attaque en premier , sinon le hero attaquera en deuxième.
    Les points de vie sont réduit par les points d'attaque.
    True redevient False pour que les personnages ne recombattent pas.
    A la fin de chaque tour ,si un personnage a attaqué , on ajoute 1 à Time_atk ou Time_atkm pour continuer ce processus.
    Les monstres peuvent avoir des competences comme poison, vol de vie etc... .
    Des competences appris dans aptitudes peuvent varier le combat comme reduire l'attaque du monstre ou parrer la premiere attaque      du monstre.
    Si les points de vie du monstre tombent à 0 , le hero gagne de l'argent et le monstre et sa position sont detruits.
    Si c'est le boss, alors  le hero a gagné.
    Sinon le hero n'a plus de vie et vous avez perdu.
    """
    global atkm_multiplicateur, Vatkm_multiplicateur, hp, mana,atk, VITatk, niveau, gold, exp, openboss, fire_skill, wall_break_skill, detect_trap_skill, heal_skill, anti_trap, action, hero_poison, boss
    #on cherche si la case est une case de monstre
    for k in range(len(monstres)):
        nb_case_monstre = monstres[k][4]
        if xkey+10*ykey ==monstres[0][4] and openboss== False: # Si le boss n'est pas encore apparue : arrete la boucle
                break
                
        elif nb_case_monstre == xkey+10*ykey :
            hpm= int(monstres[k][0])    # point de vie du monstre
            atkm= int(monstres[k][1])*atkm_multiplicateur   # point d'attaque du monstre
            Vatkm=monstres[k][2]*Vatkm_multiplicateur        # vitesse d'attaque du monstre

            atkhero=False               # quand True : le hero peut attquer
            atkmonstre=False            # quand True : le monstre peut attquer
            turn=1                      # nombre de tours du combat
            Time_atk=1                  # valeur du hero : quand cette valeur est negatif : le hero peut attaquer
            Time_atkm=1                 # valeur du monstre : quand cette valeur est negatif : le monstre peut attaquer
            parry=0                     # nombre de coup que le hero peut bloquer
            
            evade=4                     # competence d'esquive des monstres : chance d'esquiver 
            lifesteal=0                 # competence vol de vie des monstres
            
            if "lifesteal" in monstres[k][3]: # si le monstre a la competence vol de vie
                lifesteal=0.5+round(niveau*0.05,1)
                
            if shield_skill==True :           # si le hero a la competence Shield
                parry=1
            ##Combats avec vitesse attaque
            while hp>0 and hpm>0: #boucle de combat
                Time_atk=round(Time_atk-Vatk,2)
                Time_atkm=round(Time_atkm-Vatkm,2)
                esquive=0                        # quand esquive est egal a zero l'attaque du hero touche 100% 
                if "evade" in monstres[k][3]:    # Si le monstre a la competence d'esquive
                    esquive=choice(range(evade)) 
                    
                if "poison" in monstres[k][3] and hero_poison == False:    # Si le monstre a la competence poison et que le hero                   n'est pas empoisonne
                    if randint(0,4) == 0: # il y a une probabilite que le hero soit empoisonne
                        hero_poison = True
                
                ##quand les 2 peuvent attaquer
                while Time_atk<=0 or Time_atkm <=0:

                    In_history.insert(END,"     Tour {}: \n".format(turn)) # Nb de tours
                    In_history.see("end")
                    action = True

                    if Time_atk <=0: # si inferieur : le hero peut attaquer
                        atkhero=True 
                    if Time_atkm <=0 and parry <= 0:# si inferieur : le monstre peut attaquer et son attaque n'est pas parrie
                        atkmonstre = True
                        
                    elif Time_atkm <=0 :
                        parry-=1
                        In_history.insert(END,"Vous avez annule l'attaque du monstre! \n")
                        In_history.see("end")
                        
                    if  atkhero == True and atkmonstre == True:
                        if Time_atk - Time_atkm <=0 : #l'hero attaque en premier
                        
                            if esquive==evade-1:
                                In_history.insert(END,"Le {} a esquive l'attaque !\n".format(monstres[k][3][0]))
                                In_history.see("end")
                            else:
                                hpm-=round(atk*atk_multiplicateur)     # Reduit les points de vie du monstre
                                In_history.insert(END,"L'hero attaque: \n HP du {} : {}HP \n".format(monstres[k][3][0],str(hpm)))
                                In_history.see("end")
                            atkhero=False
                            if hpm <=0:               # Si les points de vie du monstre <0 alors on arrete l'attaque du monstre
                                atkmonstre=False

                            if atkmonstre== True:
                                hp-=atkm             # Reduit les points de vie du hero
                                In_history.insert(END,"Puis le {} attaque: \n HP du joueur: {}HP \n".format(monstres[k][3][0], str(hp)))
                                In_history.see("end")
                                hpm=round(hpm+atkm*lifesteal) #Si le monstre a la competence de vol de vie 
                                if lifesteal !=0:
                                    In_history.insert(END,"Le {} se soigne de {} HP -> {}HP \n" .format(monstres[k][3][0], round(atkm*lifesteal),hpm))
                                    In_history.see("end")
                                atkmonstre=False

                        if Time_atk - Time_atkm >0 : #le monstre attaque en premier
                            hp-=atkm
                            atkmonstre=False
                            In_history.insert(END,"Le {} attaque : \n HP du joueur: {}HP \n".format(monstres[k][3][0],str(hp)))
                            In_history.see("end")
                            hpm=round(hpm+atkm*lifesteal)
                            if lifesteal !=0:
                                In_history.insert(END,"Le {} se soigne de {} HP -> {}HP \n".format(monstres[k][3][0], round(atkm*lifesteal), hpm))
                                In_history.see("end")

                            if hp<=0:                # Si les points de vie du hero <0 alors on arrete l'attaque du hero
                                atkhero=False
                            if atkhero== True:  
                                if esquive==evade-1:
                                    In_history.insert(END,"Le {} a esquive l'attaque !\n".format(monstres[k][3][0]))
                                    In_history.see("end")
                                else:
                                    hpm-=round(atk*atk_multiplicateur)              
                                    In_history.insert(END,"Puis l'hero attaque : \n HP du {}: {}HP \n".format(monstres[k][3][0], str(hpm)))
                                    In_history.see("end")
                                atkhero=False
                                
                    #quand seul l'un des 2 peut attaquer
                    if atkhero == True:     #hero attaque
                        if esquive==evade-1:
                            In_history.insert(END,"Le {} a esquive l'attaque !\n".format(monstres[k][3][0]))
                            In_history.see("end")
                        else:
                            hpm-=round(atk*atk_multiplicateur)                
                            In_history.insert(END,"L'hero attaque : \n HP du {}: {}HP \n".format(monstres[k][3][0],str(hpm)))
                            In_history.see("end")
                        atkhero=False

                    if atkmonstre== True:   #monstre attaque
                        hp-=atkm
                        In_history.insert(END,"Le {} attaque : \n HP du joueur: {}HP \n".format(monstres[k][3][0],str(hp)))
                        In_history.see("end")
                        hpm=round(hpm+atkm*lifesteal)
                        if lifesteal !=0:
                            In_history.insert(END,"Le {} se soigne de {} HP -> {}HP \n".format(monstres[k][3][0], round(atkm*lifesteal),hpm))
                            In_history.see("end")
                        atkmonstre=False
                    
                    #Remettre le compteur d'attaque
                    if Time_atk<=0:
                        Time_atk+=1
                    if Time_atkm<=0:
                        Time_atkm+=1
                    turn+=1 #Tour suivant
                    
                # destruction du monstre
                if hpm<=0:
                    gold_earn=randint(25,50)+10*niveau  # Gagne de l'argent
                    exp_earn=randint(50,75)+10*niveau   # Gagne de l'experience
                    gold+=gold_earn                     # Pour afficher l'argent gagne
                    exp+=exp_earn                       # Pour afficher l'experience gagne
                    In_history.insert(END,"le monstre est mort. \n +{}exp \n +{}gold \n".format(exp_earn,gold_earn))
                    In_history.see("end")
                    hero(xkey,ykey)
                    
                    # dire que le hero est empoisonne s' il l'est   
                    if hero_poison == True : 
                        In_history.insert(END,"Le {} vous a empoisone ! \n".format(monstres[k][3][0]))
                        In_history.see("end")  
                        
                    #quand le monstre meurt
                    for x in PositionM:
                        if nb_case_monstre == x != monstres[0][4] :
                            PositionM.remove(nb_case_monstre)
                            monstres[k][4]=-999 #pour garder le nombre d'objet dans la liste
                    
                    # quand le boss est mort:
                    if nb_case_monstre == monstres[0][4]:
                        statistique()
                        showinfo("Win","Vous avez gagné")
                        gold+=100*niveau
                        exp+=150*niveau
                        niveau+=1
                        Donjon.withdraw() # eteindre l'interface du donjon
                        menuprincipale()  # retour au menu
                    
                    """Lors du donjon final:"""
                    if donjon_type != "normal": # Si un monstre meurt, boss final perd de la vie
                        monstres[0][0]-=1
                        boss()
                        if monstres[0][0] == 0 : # Si le boss final n'a plus de vie : GAGNER!
                            showinfo("Congratulation!!!"," Vous avez battu le boss final !!! \n Vous avez fini le jeu ! \n Felicitation!!!")
                            
    # Quand assez de monstres est tue : le boss se montre lors du donjon normal
    if donjon_type == "normal":
        if len(PositionM)<5 and openboss==False:
            boss()
            openboss=True

    statistique()
    if hp <=0 :# Quand joueur meurt (hp<=0)
        death()
##Mort du personnage:
def death():
    '''
    Lorsque le hero meurt : 2 choix s'offre alors au joueur .
    1er : recommencer le jeu 
    Cela va remettre tous les changements de variables  par defaut et revenir a l accueil.
    2e : eteindre le jeu.
    '''
    global hp ,mana,atk , VITatk , endurance, niveau , gold , exp ,openboss , openskill, OPENaptitude, fire_skill ,wall_break_skill ,detect_trap_skill ,heal_skill ,anti_trap ,atkm_multiplicateur ,Vatkm_multiplicateur ,arme_equip, arme ,arme_achete
    if askretrycancel("GAME OVER","Vous avez perdu. ""Recommencer?"):
        #Si recommence : remet a zero tous les changements des variables
        Accueil.deiconify()
        Donjon.withdraw()
        #stats
        hp=200
        mana=100
        atk=10
        atk_multiplicateur = 1
        VITatk=0.7
        endurance=100
        gold=0
        exp=0
        niveau=1
        #magasin
        arme_equip=0
        arme=1
        arme_achete=[]
        #aptitudes
        openskill=False
        OPENaptitude=False
        atkm_multiplicateur=1
        Vatkm_multiplicateur=1
        heal_skill=False
        detect_trap_skill=False
        wall_break_skill=False
        fire_skill=False
    else:
        Donjon.destroy()

##Sauvegarde
def sauvegarde():
    """ La fonction sauvegarde va créer un fichier dans le dossier destination du programme ou l'éditer s'il est déjà présent. La valeur de toutes les variable et des armes acheté y sont inscrit sur des lignes différentes. """
    
    f = open('sauvegarde', 'w') # Creer ou ouvrir un document text
    
    #enregistre des floats
    f.write("{} \n".format(hp)) #écrit la valeur de hp dans le fichier puis saute une ligne
    f.write("{} \n".format(mana))
    f.write("{} \n".format(atk))
    f.write("{} \n".format(VITatk))
    f.write("{} \n".format(endurance))
    f.write("{} \n".format(gold))
    f.write("{} \n".format(niveau))
    f.write("{} \n".format(exp))
    f.write("{} \n".format(arme_equip))
    f.write("{} \n".format(arme))
    f.write("{} \n".format(atk_multiplicateur ))
    f.write("{} \n".format(anti_trap))
    f.write("{} \n".format(atkm_multiplicateur))
    f.write("{} \n".format(Vatkm_multiplicateur))   
    
    
    # enregistre des listes
    f.write("{} \n".format(arme_achete))  
    
    # enregistre des str  
    f.write("{} \n".format(OPENaptitude))
    f.write("{} \n".format(heal_skill))
    f.write("{} \n".format(detect_trap_skill))
    f.write("{} \n".format(wall_break_skill))
    f.write("{} \n".format(fire_skill))
    f.write("{} \n".format(hp_regen))
    f.write("{} \n".format(mana_regen))
    f.write("{} \n".format(shield_skill))
    f.write("{} \n".format(openskill))

    
    f.close() # fermer le document
    
    showinfo("System","Votre partie a ete sauvegarde.")
    

# écris dans un document texte (qui se cré si non présent) les valeurs de toutes les variables, pour améliorer : mettre toute les variable dans une liste et utiliser for pour qu ça prenne moins de lignes, rendre le fichier liseable car acuellement bug

##charger une sauvegarde
def charger():
    global hp,  mana, atk, VITatk, endurance, enduperdu, gold, niveau, exp, arme_equip, arme, arme_achete, atk_multiplicateur, OPENaptitude, atkm_multiplicateur, Vatkm_multiplicateur, heal_skill, detect_trap_skill, wall_break_skill, fire_skill, hp_regen, mana_regen, shield_skill, openskill, anti_trap
    """ La fonction charger va lire le fichier créer grâce à la sauvegarde et mettre dans une liste toute les valeur écrite dans le fichier"""
    
    y = open('sauvegarde','r') # on ouvre le texte pour prendre les valeurs dedans
    charge = []                # liste qui ca contenir les valeurs qui est dans le texte
    # on reprend les floats
    for x in range (14) :
        charge_var = float(y.readline()) #enregistre la valeur de la première ligne puis de la seconde lors de la 2éme activation de la boucle, ....
        
        charge.append(charge_var)
        
    # on reprend les listes
    charge_var = y.readline()
    arme_achete = []
    charge.append(arme_achete)
    
    # on reprend les str
    for x in range(11):
        charge_var=str(y.readline())
        charge.append(charge_var[:5]) # on prend les 5 premieres lettres car sinon on aura \n dans notre str

    # On change les variables concernes
    hp = int(charge[0])
    mana = int(charge[1])
    atk = int(charge[2])
    VITatk = charge[3]
    endurance = charge[4]
    gold  = int(charge[5])
    niveau = int(charge[6])
    exp = int(charge[7])
    arme_equip = int(charge[8])
    arme = int(charge[9])
    atk_multiplicateur=charge[10]
    anti_trap=int(charge[11])
    atkm_multiplicateur=charge[12]
    Vatkm_multiplicateur=charge[13]
        
    OPENaptitude=charge[15]
    heal_skill=charge[16]
    detect_trap_skill=charge[17]
    wall_break_skill=charge[18]
    fire_skill=charge[19]
    hp_regen=charge[20]
    mana_regen=charge[21]
    shield_skill=charge[22]
    openskill=charge[23]

    y.close() # on ferme le texte
    menuprincipale() # On va vers le menu principale
    
## Le jeu
def jeu(donjon):
    '''
    Le bouton DONJON  dans le menu principale  nous enmene ici.
    On peut deplacer notre personnage avec les fleches directionnelles.
    Quand on sur une case de monstre, le combar s'engage .
    Apres plusieurs monstres tues ,le boss apparaitra aleatoirement sur une case vide .
    Quand le boss est tue , c'est la fin du donjon -> retour au menu principale.
    Des competences peuvent etre utiliser quand ils sont apprises : ils se trouvent en bas des stats. Il y a le sort heal() pour se soigner en hp, detect_trap() pour detecter les pieges, wall_break pour casser les bloquent adjacents et fire_ball() pour infliger des degats aux unites adjacents du hero mais pas a travers des murs.
    '''

    global frame1, xkey, ykey, Donjon, frame2, MScases, mana, hp, openboss, In_history, action, heal_button,  detect_trap_button, wall_button, fire_button, monstres, heal, detect_trap, wall_break, fire_ball, donjon_type
    monstres=[]
    x=0
    xkey,ykey=0,0
    openboss=False # Le boss sera accessible quand des monstres sont tues
    donjon_type=donjon # devient "normal" si c'est un donjon normal sinon devient "boss" donc le donjon final

    #Quand active un sort avec insuffisant de mana
    def low_mana():
        In_history.insert(END,"Low mana! \n")
        In_history.see("end")
        action = True
    
    #fct du sort de soin des points de vie
    def heal():
        global mana, hp
        if mana>=  heal_cost: # Cherche s'il y a assez de mana
            mana-= heal_cost
            hp+=heal_hp+niveau*5
            In_history.insert(END,"HP+{} ! \n".format(heal_hp+niveau*5))
            In_history.see("end")
            statistique()
        else:  # quand pas assez de mana
            low_mana()

    #fct detecteur de piege : detecte un piege aleatoire (peut etre un piege deja detecte)
    def detect_trap():
        global mana,detected_trap
        if donjon_type != "normal":  #si c'est au donjon final : pas de piege
            In_history.insert(END,"Pas de piege dans ce donjon! \n")
            In_history.see("end")  
            
        elif mana>= detect_cost :
            mana-=detect_cost
            trap_detected=choice(PositionP)
            if trap_detected not in detected_trap :  # si piege n'est pas encore detecte 
                detected_trap.append(trap_detected) # on ajoute le piege detecte dans une liste pour ne pas le redetecte
                piege=Canvas(frame1,bg="green",width=70,height=70) # Creer le piege
                piege.create_text(35,35,text="Trap",fill="black")
                piege.grid(column=trap_detected%10,row=trap_detected//10,sticky=N+S+W+E)
                In_history.insert(END,"Trap detected! \n")
                In_history.see("end")
                
            else: #si le piege est deja detecte
                In_history.insert(END,"Pas de piege detecte! \n")
                In_history.see("end")
            statistique()
            action = True
                
        else:
            low_mana()

    #fct destruction de mur
    def wall_break():
        global mana
        no_wall=[] # liste pour stocker les murs adjacents
        final_wall=[33,34,35,43,45,53,54,55]
        if mana>= wall_cost:
            mana-=wall_cost
            # Chercher s'il y a un mur adjacent
            for x in PositionB:
                if x== xkey+10*ykey+1 or x == xkey+10*ykey-1 or x == xkey+10*ykey-10 or x == xkey+10*ykey+10 :
                    if x in final_wall and donjon_type != "normal" : # Si c'est au donjon final, les murs autour du boss ne peuvent pas etre detruits 
                        In_history.insert(END,"Ce mur ne peut pas etre detruit! \n")
                        In_history.see("end")
                    else:
                        block=Canvas(frame1,width=70,height=70) # Supprime le canvas du mur
                        block.grid(column=x%10,row=x//10,sticky=N+S+W+E)
                        no_wall.append(x)
            
            # Supprime les murs dans la liste des murs        
            if no_wall != []:
                for y in no_wall:
                    PositionB.remove(y) 
            # s'il n'y a pas de mur adjacent     
            elif no_wall == []: 
                In_history.insert(END,"Il n'a pas de mur! \n")
                In_history.see("end")
                action = True
                mana+=wall_cost
            statistique()
        else:
            low_mana()
            
    #fct boule de feu
    def fire_ball():
        global mana , gold , exp ,openboss , niveau
        fire_range=[] # La limite dans chaque direction
        if mana>= fire_cost:
            mana-=fire_cost #cout du mana
            #Cherche le 1er mur a partir du hero dans chaque direction et on l'ajoute dans fire_range :
                #horizontale
            for x in range(xkey+10*ykey,ykey*10,-1) :       # a gauche
                if x in PositionB:
                    fire_range.append(x)
                    break
            if len(fire_range)==0:                          # s'il y en a pas, on met la valeur a l'extreme
                fire_range.append(0+10*ykey)
                
            for x in range(xkey+10*ykey,ykey*10+NBlignes) : # a droite
                if x in PositionB:
                    fire_range.append(x)
                    break
            if len(fire_range)==1:
                fire_range.append(NBcolonnes+10*ykey)
                #verticale
            for x in range(xkey+10*ykey,xkey,-10):          # en haut
                if x in PositionB:
                    fire_range.append(x)
                    break
            if len(fire_range)==2:
                fire_range.append(xkey)
            for x in range(xkey+10*ykey,xkey+10*NBcolonnes,10): # en bas
                if x in PositionB:
                    fire_range.append(x)
                    break
            if len(fire_range)==3:
                fire_range.append(xkey+NBlignes*10)

            #inflige aux monstres orthogonaux au personnage qui ne sont pas derriere un mur
            monstre_in_range=[] # Les monstres qui sont morts par la boule de feu
            monstre_touche=0     # Le nombre de monstre touche pas la boule de feu
            for x in PositionM:
                if x in range(fire_range[0],fire_range[1]) or x in range(fire_range[2],fire_range[3],10): #on cherche s'il y a un monstre dans ces 2 intervalles
                    for y in range(len(monstres)):
                        if monstres[y][4]==x : # S'il y en a un
                            if x ==monstres[0][4] and openboss== False: #si le boss n'est pas apparu
                                break
                            monstres[y][0]-=20 # On inflige des degats au monstre
                            monstre_touche+=1  # ajoute un monstre touche
                            In_history.insert(END,"Le {} a perdu 20HP -> {}HP \n".format(monstres[y][3][0],monstres[y][0]))
                            In_history.see("end")
                            #si le monstre est mort
                            if monstres[y][0]<=0 :
                                monstre_in_range.append(x)
                                if  x !=monstres[0][4]:
                                    monstres[y][4]=-999
                                void=Canvas(frame1,width=70,height=70).grid(column=x%10,row=x//10,sticky=N+S+W+E)
                                #si c est le boss qui est tue
                                if x == monstres[0][4]:
                                    showinfo("Win","Vous avez gagné")
                                    gold+=100*niveau
                                    exp+=200*niveau
                                    niveau+=1
                                    Donjon.withdraw()
                                    menuprincipale()
                                #si c est un monstre normal
                                else:
                                    gold_earn=randint(25,50)*niveau
                                    exp_earn=randint(50,75)*niveau
                                    gold+=gold_earn
                                    exp+=exp_earn
                                    In_history.insert(END,"le monstre est mort. \n +{}exp \n +{}gold\n".format(exp_earn,gold_earn))
                                    In_history.see("end")
                                    action = True
                            #si le monstre n'est pas mort
                            else:
                                monstre(y,x%10,x//10)
                                
            #On supprime les emplacements des monstres tues
            if monstre_in_range !=[]:
                for y in monstre_in_range:
                    PositionM.remove(y)
                    
            #S'il n'y a pas de monstre touche      
            if monstre_touche==0: 
                In_history.insert(END,"Il n'y a pas de monstre.\n")
                In_history.see("end")
                action = True
                mana+=fire_cost  # Rend le mana utilise
                
            #Marche que dans le donjon normal
            if donjon_type == "normal":
                if len(PositionM)<5: # Quand assez de monstres est tue:
                    boss()
                    openboss=True
                    
            statistique()
#}---------------

    Donjon=Toplevel()
    Donjon.title("MYSTERIOUS DUNGEON")
    
    Grid.rowconfigure(Donjon,0,weight=1)
    Grid.columnconfigure(Donjon,0,weight=1)

    #fenetre de Donjon
    frame0=LabelFrame(Donjon, width=720, height=720)
    Grid.rowconfigure(frame0,0,weight=1)
    Grid.columnconfigure(frame0,2,weight=1)
    frame0.grid(row=0,column=0 ,sticky=NSEW)

    #fenetre du plateau
    frame1=LabelFrame(frame0, width=720, height=720)
    Grid.rowconfigure(frame1,9,weight=1)
    Grid.columnconfigure(frame1,9,weight=1)
    frame1.grid(row=0,column=0 ,sticky=NSEW)

    #fenetre des stats et des aptitudes
    frame2=LabelFrame(frame0, width=720, height=720)
    Grid.rowconfigure(frame2,1,weight=1)
    Grid.columnconfigure(frame2,0,weight=1)
    frame2.grid(column=1 , row=0 ,sticky=NSEW)

    #fenetre de description
    frame3=LabelFrame(frame0)
    Grid.rowconfigure(frame3,1,weight=1)
    Grid.columnconfigure(frame3,1,weight=1)
    frame3.grid(column=2,row=0,sticky=N+S+W+E)

    # Menu
    menubar=Menu(Donjon)
    menu1=Menu(menubar,tearoff=0)
    menu1.add_command(label="Quitter",command=Donjon.destroy)
    menu1.add_command(label="Retour",command=lambda :(menuprincipale() , Donjon.withdraw()) )
    menubar.add_cascade(label="Partie",menu=menu1)
    Donjon.config(menu=menubar)

    #Deplacer la description:
    scroll_history=Scrollbar(frame3,width=18)
    scroll_history.grid(row=1, column=1, sticky=S+N)
    
    # description des actions
    In_hist=Label(frame3,text="History : ",width=25 ).grid(column=0,columnspan=2,row=0,sticky=N+E+W)
    In_history=Text(frame3,width=45, font=("consolas", 12), yscrollcommand= scroll_history.set)
    In_history.grid(column=0,row=1,sticky=E+W+S+N)
    scroll_history.config(command=In_history.yview)
    #Dire le fonctionnement du deplacement
    In_history.insert(END,"Deplacer avec les fleches directionnelles. \n")
    In_history.see("end")
    action=True

    # Creer les boutons d'aptitudes au fenetre central mais ils sont desactive
    heal_button=Button(frame2,text="[1] \nHeal\n {}mana".format(heal_cost),  state=DISABLED, command=heal, height=3, width=10)    
    heal_button.grid(column=0,row=2,sticky=S)
    detect_trap_button=Button(frame2,text="[2] \nDetect trap\n {}mana".format(detect_cost), state=DISABLED, command=detect_trap, height=3, width=10)
    detect_trap_button.grid(column=0,row=3,sticky=S)
    wall_button=Button(frame2,text="[3] \nWall break\n {}mana".format(wall_cost), state=DISABLED,command=wall_break,height=3,width=10) 
    wall_button.grid(column=0,row=4,sticky=S)
    fire_button=Button(frame2,text="[4] \nFire ball\n {}mana".format(fire_cost), state=DISABLED, command=fire_ball, height=3, width=10) 
    fire_button.grid(column=0,row=5,sticky=S)

    # Activer les aptitudes  au fenetre central lorsqu'ils sont apprises
    if heal_skill==True:
        heal_button.config(state="normal",fg="green")
    if detect_trap_skill== True :
        detect_trap_button.config(state="normal",fg="dark green")
    if wall_break_skill== True:
        wall_button.config(state="normal",fg="brown")
    if fire_skill == True:
        fire_button.config(state="normal", fg="orange")
           
    #Creer le cadre
    for i in range(NBlignes):
        for j   in range(NBcolonnes):
            Cases=Canvas(frame1,width=70,height=70,relief= RIDGE ).grid(column=j,row=i,sticky=NSEW)

    entity_spawn() #Creer le personnage, le boss,les obstacles, les monstres et les pieges
    statistique() #afficher les stats

    #deplacement avec le clavier dans le cadre
    frame1.focus_set()
    frame1.bind("<Key>",keyboard)
    
    #deplacement avec le clavier dans le texte
    In_history.bind("<Key>",keyboard)
    
    
    Menu_principale.withdraw() #Quitter le menu principale lorqu'on arrive dans le donjon

##La boutique
def shop():
    """ fonction du menu boutique auquel on peut accéder depuis le menue principal. Elle affiche un ensemble de bouton et valeur de variable qui sont actualiser a chaque action, on peut y "acheter" et "équiper" des armes """
    
    global boutique, arme, shop1, listearmes, armestats, armel,openshop


    #fonction pour actualiser le magasin à chaque fois qu'il y a une chat, un changement d'arme, ....
    def actushop():

        numeroarme=Label(shop1,text="{}/{}".format(arme,len(listearmes)-1)).grid(column=6,row=2)
        nom_arme=Label(shop1,text=listearmes[arme],width=15).grid(column=1,row=1)

        #affiche les stats de l'arme
        statarme=Label(shop1,text="  {}atk,{}spd,{}G  ".format(armestats[arme][0],armestats[arme][1],armestats[arme][2])).grid(column=1,row=2)
        #affiche les stats du joueur
        stat4=Label(shop1,text="STATS \n hp={} \n atk={} \n atk speed={} \n gold={} ".format(hp,atk,round(VITatk*endurance/100,2), gold)) .grid(column=10,row=1)

        achetable=True
        for x in arme_achete:
            if x == arme :
                achetable=False

        if achetable ==True:            # Si l'arme n'est pas encore achete:
            Bepee1=Button(shop1,text="acheter",fg='gold')
            Bepee1.config(command=achat)
            Bepee1.grid(column=1,row=3)

        else:

            if arme == arme_equip:      # Si l'arme n'est pas equippe
                Bepee1=Button(shop1,text="stocker",fg='red')
                Bepee1.config(command=laisser)
                Bepee1.grid(column=1,row=3)
            else :                      # Si l'arme est equippe
                Bepee1=Button(shop1,text="équiper",fg='blue')
                Bepee1.config(command=prendre)
                Bepee1.grid(column=1,row=3)
    
    #permet de changer l'arme actuellement visible
    def change_page(x): # Bouton precedent et suivant
        global arme
        if x==-1 and arme ==1: # Precedent
            arme=len(listearmes)
        if x==1 and arme==len(listearmes)-1: # Suivant
            arme=0
        arme+=x
        actushop()


    def prendre(): # fonction pour équiper une armes, on ajoute au statistique du personnage celle apporté par qu'on équipe
        global arme, shop1, listearmes, armestats, armel, gold , atk , VITatk ,arme_equip
        if arme_equip != 0:
            atk-=armestats[arme_equip][0]
            VITatk-=armestats[arme_equip][1]
        arme_equip = arme
        atk+= armestats[arme_equip][0]
        VITatk+= armestats[arme_equip][1]
        actushop()

        Bepee1=Button(shop1,text="stocker",fg='red',command=laisser).grid(column=1,row=3)


    def laisser(): # fonction pour déséquiper une arme, un enlève au statistique du personnage celle apporté par l'arme équipé
        global arme , shop1, listearmes, armestats, armel, gold , atk , VITatk,  arme_equip
        arme_equip=0
        atk-= armestats[arme][0] #ajuste la valeur d'attaque du personnage en enlevant celle donné par l'arme
        VITatk-= armestats[arme][1]
        Bepee1=Button(shop1,text="équiper",fg='blue',command=prendre).grid(column=1,row=3)
        actushop()

    #fonction pour acheter une arme
    def achat():
        global arme, shop1, listearmes, armestats, armel, gold , atk , VITatk
        if gold >= armestats[arme][2]: #vérifie que le joueur a assez d'argent pour acheter l'objet
            gold= gold- armestats[arme][2]
            arme_achete.append(arme)
            Bepee1=Button(shop1,text="équiper",fg='blue',command=prendre).grid(column=1,row=3)
            actushop()

        else:
            showinfo('Attention!',"Vous n'avez pas assez d'argent.")

#}---------

    boutique=Toplevel()
    boutique.title("MYSTERIOUS DUNGEON")

    Grid.rowconfigure(boutique,0,weight=1)
    Grid.columnconfigure(boutique,0,weight=1)

    shop1=Canvas(boutique)
    shop1.grid()


    listearmes=["","baton","epee","hache","lance","  arc  ","rapier", "Excalibur"]                       # Noms des armes
    armestats=["",[3,0.1,300],[3,0.2,600],[15,-0.3,800],[10,0,800],[-5,0.5,1000],[-8,1,2000],[50,0,10000]]  # Stats des armes: {atk, Vatk, cout}

    suivant=Button(shop1,text="suivant",command=lambda x=1:change_page(x)).grid(column=6,row=1)          # Bouton suivant
    precedent=Button(shop1,text="précédent",command=lambda x=-1: change_page(x)).grid(column=0,row=1)    # Bouton precedent
    Bepee1=Button(shop1,text="acheter",fg='gold',command=achat).grid(column=1,row=3)                     # Achat de l'arme
    
    #Stats de l'arme
    statarme=Label(shop1,text="{}atk,{}spd,{}G".format(armestats[arme][0],armestats[arme][1],armestats[arme][2])).grid(column=1,row=2)
    nom_arme=Label(shop1,text=listearmes[arme],width=15).grid(column=1,row=1)                            # Nom de l'arme
    numeroarme=Label(shop1,text="{}/{}".format(arme,len(listearmes)-1)).grid(column=6,row=2)             # Numero de l'arme

    #affiche les stats du joueur
    stat4=Label(shop1,text="STATS \n hp={} \n atk={} \n atk speed={} \n gold={} ".format(hp,atk,round(VITatk*endurance/100,2),gold)) .grid(column=10,row=1)
    # bouton qui permet de quitter la boutique
    bou_sortir = Button(shop1,text='sortir',bg="grey",command= lambda:(boutique.withdraw(), menuprincipale())).grid(column=0,row=3)

    actushop()
    Menu_principale.withdraw() #Quitter le menu principale lorqu'on arrive dans le magasin

##Aptitudes
def skill():

    '''
    On peut se rendre ici depuis le menu principale .
    Ici , on peut apprendre des competences differentes avec de l'experience : exp .
    On peut apprendre les competence si on a assez d'experience :l'exp besoin est : expneeded.
    Des competence peut etre utiliser lors du donjon.
    '''
    global skillset,openskill,special1,special2,special3,special4, special5,special6,special7,special8


    #Si la page des aptitudes est deja ouvert une fois
    if openskill==True:
        skillset.deiconify()
        stat=Label(skillset,text="hp={} \n mana={} \n atk={} \n atk speed={} \n exp={}".format(round(hp) ,mana,round(atk*atk_multiplicateur), round(VITatk*endurance/100,2),exp)).grid(column=14,row=0 ,sticky=N)
    #Ouvrir la page des aptitudes
    else:
        openskill=True# La fenetre disparaitra quand on le quitte

        #Debloquer l'arbre d'aptitude
        def learn1():

            for nb,spx,spy in zip(range(0,4),skillx,skilly) :
                ButtonRank[nb].config(state='normal')
                ButtonRank[nb].grid(row=sps+spy, column=sps+spx, sticky=N+S+E+W)
                aptitude1.config(state=DISABLED)

        #apprendre les aptitudes
        def learn(x):
            global hp,mana,atk,VITatk,special1,special2,special3,special4,special5,special6,special7,special8, exp,anti_trap ,skillopen,detect_trap_skill,heal_skill , wall_break_skill ,fire_skill, mana_regen, hp_regen, atk_multiplicateur, shield_skill,Vatkm_multiplicateur, atkm_multiplicateur

            learn_skill=True # Si true la competence est appris
            
            """Chaque ligne correspond au rang de la competence : rang 1, rang 2, rang 3 et rang 4"""
            var_change=[atk, hp, VITatk, mana,                                                                                                            0, atk_multiplicateur, 0, shield_skill, 0, Vatkm_multiplicateur, fire_skill, 0,                                                 0,hp_regen, anti_trap, mana_regen,                                                                                                                 detect_trap_skill, wall_break_skill, atkm_multiplicateur, heal_skill]                                               # Les variables a changer, les 0 sont des variables qui sont deja mentionnees donc au lieu de changer la variable on ajoute une valeur a la variable
            new_var=[atk+10,hp+100,VITatk+0.05,mana+50,                                                                                   10,1.2,200,True,0.1,0.8,True,200,                                                                                                              1,True,anti_trap+3,True,                                                                                                      True,True,0.8,True]  #Les changements aux variables concernes
            cout_exp=[expneeded,expneeded,expneeded,expneeded,                                   expneeded2,expneeded2,expneeded2,expneeded2,expneeded2,expneeded2,expneeded2,expneeded2,   expneeded3,expneeded3,expneeded3,expneeded3,expneeded4,                                                              expneeded4,expneeded4,expneeded4] # cout des competences
           
            unlock_button=[4,6,8,10,5,7,9,11] # Les boutons a activer
            special_change1=[0,0,1,1,2,2,3,3] #} Portes AND
            special_change2=[4,6,5,7,4,5,6,7] #}
            
            """On cherche d'abord si on a assez d'experience puis on change transforme tous les anciens variables en nouvelle."""  
            
            if exp- cout_exp[x] <= 0: # Rien ne se passe si on n'a pas assez d'experience
                showinfo('Attention!',"Manque d'experience.")
                learn_skill=False     # on ne fait pas apprendre la competence
            else:                       # Si on a assez d'experience
                exp-= cout_exp[x]       # On paye l'experience
                var_change[x]=new_var[x]# On change les valeurs en fonctions de la competence appris 
                
                # On rafraichit les variables
                atk=var_change[0]
                hp=var_change[1]
                VITatk=var_change[2]
                mana=var_change[3]
                atk+=var_change[4]
                atk_multiplicateur=var_change[5]
                hp+=var_change[6]
                shield_skill=var_change[7]
                VITatk+=var_change[8]
                Vatkm_multiplicateur=var_change[9]
                fire_skill=var_change[10]
                mana+=var_change[11]
                atk_multiplicateur+=var_change[12]
                hp_regen=var_change[13]
                anti_trap=var_change[14]
                mana_regen=var_change[15]
                detect_trap_skill=var_change[16]
                wall_break_skill=var_change[17]
                atkm_multiplicateur=var_change[18]
                heal_skill=var_change[19]
                
                # on active les boutons de rang 2 grace aux boutons de rang 1
                if 0<= x <=3 :
                    ButtonRank[unlock_button[x]].config(state='normal'), ButtonRank[unlock_button[x+4]].config(state='normal')
                # on active les boutons de rang 3 et 4 grace aux boutons de rang 2
                elif 4 <= x <= 11 :
                    Sspecial[special_change1[x-4]]+=1
                    Sspecial[special_change2[x-4]]+=1
                    
                    #Ouvrir aptitudes rang 3 et 4 lorsque un special est egal a 2
                    for i,j in zip(Sspecial,range(0,8)):
                        if i ==2:
                            ButtonRank[j+12].config(state='normal')
                            Sspecial[j]=0
            # Montrer les stats
            stat=Label(skillset,text="hp={} \n mana={} \n atk={} \n atk speed={} \n exp={}".format(round(hp) ,mana,round(atk*atk_multiplicateur), round(VITatk*endurance/100,2),exp)).grid(column=14,row=0 ,sticky=N)
            #N'apprend pas l'aptitude si learn_skill==False
            if learn_skill==True:
                ButtonRank[x].config(state=DISABLED)   #Rendre le bouton appris inaccessible quand appris
                showinfo("Succes!","Skill : < {} >  learned".format(skill_name[x][0])) #Aptitudes appris
                learn_skill==False
#}-----------------
    
    
        skillset=Toplevel()
        skillset.title("MYSTERIOUS DUNGEON")
    
        Grid.rowconfigure(skillset,14,weight=1)
        Grid.columnconfigure(skillset,14,weight=1)
    
        skillset1=Canvas(skillset)
        skillset1.grid()
    
        openskill=True #La prochaine fois qu'on ouvre cette interface , il va rester le meme au lieu de reinitialiser
    
        #Afficher les stats
        stat=Label(skillset,text="hp={} \n mana={} \n atk={} \n atk speed={} \n exp={}".format(round(hp) ,mana,round(atk*atk_multiplicateur), round(VITatk*endurance/100,2),exp)).grid(column=14,row=0 ,sticky=N)
    
            #Bouton pour ouvrir aptitudes rang 1
        if OPENaptitude==False:
            aptitude1=Button(skillset1,text="OPEN!",width=10,height=2)
            aptitude1.config(command=learn1)
            aptitude1.grid(row=sps,column=sps)
    
            #aptitudes niveau 1,2,3,4
            for nb,spx,spy in zip(range(0,20),skillx,skilly) :
                ButtonRank[nb]=Button(skillset1,text=skill_name[nb][0]+"\n"+ "(cost: {} )".format(skill_name[nb][1]),state=DISABLED)
                ButtonRank[nb].config(command=lambda x=nb: learn(x) )
                ButtonRank[nb].grid(row=sps+spy, column=sps+spx, sticky=N+S+E+W)
            OPENsptitude=True
            
        #Lignes dans les aptitudes:
        lineH=[[6,5],[6,7],[2,3],[2,4],[10,3],[10,4],[2,8],[2,9],[10,8],[10,9]] #rectangle horizontale
        lineV=[[5,6],[7,6],[3,2],[4,2],[3,10],[4,10],[8,2],[9,2],[8,10],[9,10]] #rectangle verticale
        lineHD=[[3,5],[7,9],[7,0],[12,5]]                                       #rectangle haut-droite
        lineHG=[[3,7],[7,12],[7,3],[12,7]]                                      #rectangle haut-gauche             
        lineBD=[[5,0],[0,5],[5,9],[9,5]]                                        #rectangle bas-droite 
        lineBG=[[5,3],[0,7],[5,12],[9,7]]                                       #rectangle bas-gauche 

        
        # C'est pour creer les chemins des competences pour les amener au prochaines competences, ce sont desrectangles
        for x,y in lineH:
            line=Canvas(skillset1,width=60,height=20)
            line.create_rectangle(0,5,60,15,fill="black")
            line.grid(row=x,column=y)
        for x,y in lineV:
            line=Canvas(skillset1,width=60,height=40)
            line.create_rectangle(25,0,35,40,fill="black")
            line.grid(row=x,column=y)
        
        for x,y in lineHD:
            line=Canvas(skillset1,width=60,height=40)
            line.create_rectangle(25,0,35,25,fill="black")
            line.create_rectangle(35,15,60,25,fill="black")
            line.grid(row=x,column=y)
            
        for x,y in lineHG:
            line=Canvas(skillset1,width=60,height=40)
            line.create_rectangle(25,0,35,25,fill="black")
            line.create_rectangle(0,15,35,25,fill="black")
            line.grid(row=x,column=y)
            
        for x,y in lineBD:
            line=Canvas(skillset1,width=60,height=40)
            line.create_rectangle(25,25,35,40,fill="black")
            line.create_rectangle(25,15,60,25,fill="black")
            line.grid(row=x,column=y)
        for x,y in lineBG:
            line=Canvas(skillset1,width=60,height=40)
            line.create_rectangle(25,25,35,40,fill="black")
            line.create_rectangle(0,15,35,25,fill="black")
            line.grid(row=x,column=y)
            
        
        # Ici ce sont aussi des chemins mais on a assemble 2 rectangles : un horizontale et un verticale    
        line=Canvas(skillset1,width=80,height=40)
        line.create_rectangle(35,20,45,40,fill="black")
        line.create_rectangle(0,15,80,25,fill="black")
        line.grid(row=3,column=6)
        
        line=Canvas(skillset1,width=80,height=40)
        line.create_rectangle(35,0,45,20,fill="black")
        line.create_rectangle(0,15,80,25,fill="black")
        line.grid(row=9,column=6)
        
        line=Canvas(skillset1,width=60,height=40)
        line.create_rectangle(25,0,35,40,fill="black")
        line.create_rectangle(30,15,60,25,fill="black")
        line.grid(row=6,column=3)
        
        line=Canvas(skillset1,width=60,height=40)
        line.create_rectangle(25,0,35,40,fill="black")
        line.create_rectangle(0,15,30,25,fill="black")
        line.grid(row=6,column=9)
        
        #Bouton pour sortir
        exitskill = Button(skillset,text='sortir',bg="grey",command=lambda : (menuprincipale(), skillset.withdraw())) .grid(column=14,row=14,sticky=S)
    
    Menu_principale.withdraw() #Quitter le menu principale lorqu'on arrive dans les aptitudes(debut1 > fin2 or fin1 < fin2)
    
##Menus principale ##2e fenetre
def menuprincipale():
    """C'est la focntion principale du jeu. Il y a 7 boutons: donjon pour gagner de l'argent et de l'experience, donjon final qui sera le dernier donjon a vaincre , boutique pour acheter des armes, aptitudes pour apprendre des competences, sauvegarde pour sauvegarder la partie ,"god mode" sert a tester le jeu et quitter pour quitter le jeu """

    #GOD MODE
    def god_mode():
        
        """Cette fonction sert a tester le jeu, on a tous les sorts, beaucoup de vie, de mana, d'argent et d'experience """
        global hp, mana, atk, gold, exp, donjonfinal, detect_trap_skill, heal_skill, wall_break_skill, fire_skill, niveau

        hp=100000
        mana=10000
        atk=10000
        gold=10000
        exp=100000(debut1 > fin2 or fin1 < fin2)
        endurance=100
        niveau=3
        detect_trap_skill=True
        heal_skill=True
        wall_break_skill=True
        fire_skill=True

        stat=Label(framepp,text="STATS \n hp={} \n mana={} \n atk={} \n atk speed={} \n gold={} \n exp={} \n endurance={}/100" .format(round(hp),mana,round(atk*atk_multiplicateur),round(VITatk*endurance/100,2),gold,exp,round(endurance,1))).grid(row=0,rowspan=3,column=2)

#}------------
    global Menu_principale,frame1

    Menu_principale = Toplevel()
    Menu_principale.title("MYSTERIOUS DUNGEON")

    #Menu
    menubar=Menu(Menu_principale)
    menu1=Menu(menubar,tearoff=0)
    menu1.add_command(label="Quitter",command=Menu_principale.destroy)
    menubar.add_cascade(label="Partie",menu=menu1)
    Menu_principale.config(menu=menubar)

    Grid.rowconfigure(Menu_principale,0,weight=1)
    Grid.columnconfigure(Menu_principale,0,weight=1)
    framepp=LabelFrame(Menu_principale,width=500, height=500)
    Grid.rowconfigure(framepp,8,weight=1)
    Grid.columnconfigure(framepp,8,weight=1)
    framepp.grid(sticky=NSEW)

    #Afficher les stats
    stat=Label(framepp,text="STATS \n hp={} \n mana={} \n atk={} \n atk speed={} \n gold={} \n exp={} \n endurance={}/100" .format(round(hp), mana,round(atk*atk_multiplicateur),round(VITatk*endurance/100,2),gold,exp,round(endurance,1))).grid(row=0,rowspan=3,column=2)

    #Bouton pour experimenter tous les fcts:
    GOD_MODE=Button(framepp,text="INVICIBLE",fg="violet",relief=RAISED,command=god_mode,width=30,height=4).grid(row=3,column=2)
    # bouton qui ouvre une boutique
    Mboutique=Button(framepp,text="BOUTIQUE",fg='blue',command=shop,width=30,height=4).grid(column=0,row=2)
    # bouton pour les aptitudes
    aptitude=Button(framepp,text="APTITUDES",fg='blue',command=skill,width=30,height=4).grid(column=1,row=2)
    # bouton pour sauvegarder la partie
    sauvegarder=Button(framepp,text="SAUVEGARDER",command=sauvegarde,width=30,height=4).grid(column=0,row=3)
    # bouton pour quitter le jeu
    quitter=Button(framepp,text="QUITTER",command=Menu_principale.destroy,width=30,height=4).grid(column=1,row=3)
    # bouton pour lancer le 1er donjon(debut1 > fin2 or fin1 < fin2)
    donjon=Button(framepp,text="DONJON",fg='red',command=lambda:jeu("normal"),width=30,height=4).grid(column=0,row=1)
    # bouton pour lancer le combat contre le boss final mais il est desactive
    donjonfinal=Button(framepp,text="BOSS FINAL",width=30,height=4,state=DISABLED)
    donjonfinal.grid(column=1,row=1)
    
    #Activer le bouton du boss finale quand on est arrive a un certain niveau
    if niveau>=3:
        donjonfinal.config(state="normal",fg='red', command=lambda:jeu(None))
        
    Accueil.withdraw() #Fermer la fenetre d'accueil

##Debut du jeu ## 1er fenetre
'''
Cette interface sert d'ecran d'accueil. Il permet d'entrer dans le jeu ou de reprendre la derniere partie sauvegarde.
On peut aussi quitter le jeu.
'''
Accueil = Tk()
Accueil.title("MYSTERIOUS DUNGEON")

Grid.rowconfigure(Accueil,0,weight=1)
Grid.columnconfigure(Accueil,3,weight=1)

# Titre
titre=Label(Accueil,text="MYSTERIOUS DUNGEON",bg="gray",fg="brown",font=("consolas", 40),height=2,width=20).grid(sticky=N+S+E+W)
# Bouton commencer la partie
bou_game = Button(Accueil,text='Nouvelle partie',height=5,width=40,bg="grey",command=menuprincipale).grid(sticky=N+S+E+W)
# Bouton reprendre la partie
bou_charger = Button(Accueil,text="Continuer",height=5,width=40,bg="grey",command=charger).grid(sticky=N+S+E+W)
# Bouton quitter le jeu
bou_quitter = Button(Accueil,text='Quitter',height=5,width=40,bg="grey",command=Accueil.destroy).grid(sticky=N+S+E+W)

Accueil.mainloop()